from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import CreateProjectForm

# from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {"project": project}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {"project": project}
    return render(request, "projects/show_project.html", context)


def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
