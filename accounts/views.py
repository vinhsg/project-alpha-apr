from django.shortcuts import render, redirect
from accounts.forms import LogInForm, SignUpForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


# Create your views here.


def loginview(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LogInForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def logoutview(request):
    logout(request)
    return redirect("login")


def signupview(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            user = authenticate(request, username=username, password=password)
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("Your password do not match, please try again!")
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "registration/signup.html", context)
